import 'package:flutter/material.dart';

abstract class DialogHelper {
  static IDialog defaultErrorDialog = DefaultErrorDialog();
}

abstract class IDialog {
  Widget show(BuildContext context, Widget widget);
}

class DefaultErrorDialog implements IDialog {
  @override
  Widget show(BuildContext context, Widget widget) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
        child: widget,
      ),
    );
  }
}
