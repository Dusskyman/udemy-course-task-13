import 'package:flutter/material.dart';
import 'package:udemy_course_task_13/custom_widget/dialogs/dialog_helper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DialogPage(),
    );
  }
}

class DialogPage extends StatelessWidget {
  Widget customDialog = Container(
    alignment: Alignment.center,
    width: 200,
    height: 100,
    padding: EdgeInsets.only(top: 10),
    color: Colors.white,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text('Alert Dialog'),
        TextButton(onPressed: () {}, child: Text('Ok'))
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dialog Helper demonstaration'),
      ),
      backgroundColor: Colors.grey[400],
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.open_in_full),
        onPressed: () {
          DialogHelper.defaultErrorDialog.show(context, customDialog);
        },
      ),
    );
  }
}
